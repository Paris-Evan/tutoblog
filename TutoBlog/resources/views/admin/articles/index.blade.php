@extends('admin.admin')


@section('content')
    <h1>Articles</h1>
    <a href="{{route('admin.article.create')}}">créer</a>

    <table class="'table table-striped'">
        <tr>
            <th>Titre</th>
            <th>Auteur</th>
            <th>Image</th>
            <th>Description</th>

        </tr>

        <tbody>
        @foreach($articles as $article)
            <tr>
                <td>{{$article->title}}</td>
                <td>{{$article->description}}</td>
                <td>{{$article->author}}</td>
                <td>{{$article->image}}</td>

            </tr>

        @endforeach

        </tbody>
    </table>
    {{$articles->links()}}
@endsection
